/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Executor;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Samuel
 */
public class UDPCommunicator implements ITimeoutHandler, IReceivedHandler {

    private final static int MAX_RESENT = 5;

    public class DeferredSendResult {

        private UDPSendResult _current_result;

        private DeferredSendResult() {
            _current_result = UDPSendResult.NOT_SENT;
        }

        private void setResult(UDPSendResult res) {
            _current_result = res;
        }

        public UDPSendResult getResult() {
            return _current_result;
        }

        public void WaitForResult() {
            while (_current_result == UDPSendResult.NOT_SENT) {
                try {
                    Thread.sleep(50);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }
    }

    private class QueuedSend {

        private final Serializable data;
        private final DeferredSendResult associatedResult;

        private QueuedSend(Serializable data, DeferredSendResult assRes) {
            this.data = data;
            this.associatedResult = assRes;
        }
    }

    private ConcurrentHashMap<Host, Queue<QueuedSend>> _sending_queue;
    private ConcurrentHashMap<Host, Integer> _sending_sequence_number_mapping;
    private ConcurrentHashMap<Host, Integer> _receiving_sequence_number_mapping;
    private Queue<Object> _received_buffer;
    private ConcurrentHashMap<Host, TimeoutHandler> _timeout_handlers;
    private ExecutorService _thread_pool;
    private int _port;
    private DatagramSocket _socket;
    private UDPListener _current_listener;

    public UDPCommunicator(int port) throws SocketException {
        _received_buffer = new ConcurrentLinkedQueue<>();
        _sending_sequence_number_mapping = new ConcurrentHashMap<>();
        _receiving_sequence_number_mapping = new ConcurrentHashMap<>();
        _sending_queue = new ConcurrentHashMap<>();
        _timeout_handlers = new ConcurrentHashMap<>();
        _thread_pool = Executors.newCachedThreadPool();
        _socket = new DatagramSocket(port);
        _port = port;
    }

    public void startListening() {
        _current_listener = new UDPListener((IReceivedHandler) this, _socket);
        _thread_pool.execute(_current_listener);
    }

    public void shutdown() {
        _current_listener.stopListening();
        _thread_pool.shutdown();
        try {
            _thread_pool.awaitTermination(1, TimeUnit.MINUTES);
        } catch (Exception ex) {
            _thread_pool.shutdownNow();
        }

    }

    private void addToSendingQueue(Host dest, Serializable data, DeferredSendResult assRes) {
        QueuedSend qs = new QueuedSend(data, assRes);
        if (!_sending_queue.containsKey(dest)) {
            Queue<QueuedSend> temp = new ConcurrentLinkedQueue<>();
            temp.add(qs);
            _sending_queue.put(dest, temp);
        } else {
            _sending_queue.get(dest).add(qs);
        }
    }

    public DeferredSendResult Send(Host dest, Serializable data) {
        DeferredSendResult res = new DeferredSendResult();
        if (_sending_sequence_number_mapping.containsKey(dest)) {

            if (_timeout_handlers.containsKey(dest)) {
                addToSendingQueue(dest, data, res);
            } else {
                UDPMessage m = new UDPMessage(_sending_sequence_number_mapping.get(dest), -1, data, UDPMessageType.DATA, res);
                RawSend(dest, m, true);
            }
        } else {
            addToSendingQueue(dest, data, res);
            UDPMessage m = new UDPMessage(-1, -1, null, UDPMessageType.SYNC);
            RawSend(dest, m, true);
        }
        return res;
    }

    public Object Receive() {
        while (_received_buffer.isEmpty()) {
            try {
                Thread.sleep(50);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
        return _received_buffer.remove();
    }

    private boolean RawSend(Host dest, UDPMessage m, boolean canTimeout) {
        try {
            byte[] buffer = m.ToBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(dest.getHostname()), dest.getPort());
            _socket.send(packet);
            if (canTimeout) {
                TimeoutHandler toh = new TimeoutHandler(dest, m, this);
                _timeout_handlers.put(dest, toh);
                _thread_pool.execute(toh);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public void TimedOut(Host dest, UDPMessage m) {
        TimeoutHandler temp = _timeout_handlers.remove(dest);
        temp.Stop();
        m.incrementResent();
        if (m.getResentCount() > MAX_RESENT) {
            if (m.getType() == UDPMessageType.SYNC) {
                //If syncying, then first message should be the message we were sending;
                _sending_queue.get(dest).remove().associatedResult.setResult(UDPSendResult.TIMED_OUT);
            } else {
                m.getAssociatedResult().setResult(UDPSendResult.TIMED_OUT);
            }
        } else {
            RawSend(dest, m, true);
        }
    }

    @Override
    public void UDPMessageReceived(Host src, UDPMessage m) {
        if (m.getType() == UDPMessageType.ACK) {
            System.out.println("ACK");
            if (_timeout_handlers.containsKey(src)) {
                TimeoutHandler temp = _timeout_handlers.remove(src);
                temp.Stop();
                _sending_sequence_number_mapping.put(src, _sending_sequence_number_mapping.get(src) + 1);
                temp.getAssociatedMessage().getAssociatedResult().setResult(UDPSendResult.OK);
            }
        } else if (m.getType() == UDPMessageType.DATA) {
            System.out.println("DATA");
            if (_receiving_sequence_number_mapping.containsKey(src)) {
                int currentExpectedSeq = _receiving_sequence_number_mapping.get(src) + 1;
                if (m.getSequenceNumber() == currentExpectedSeq) {
                    _received_buffer.add(m.getData());
                    _receiving_sequence_number_mapping.put(src, currentExpectedSeq);
                    UDPMessage ack = new UDPMessage(-1, currentExpectedSeq, null, UDPMessageType.ACK);
                    RawSend(src, ack, false);
                } else {
                    //only other case is that it's a duplicate, so we re-ack
                    UDPMessage ack = new UDPMessage(-1, _receiving_sequence_number_mapping.get(src), null, UDPMessageType.ACK);
                    RawSend(src, ack, false);
                }
            } else {
                UDPMessage resync = new UDPMessage(-1, -1, null, UDPMessageType.RESYNC_REQU);
                RawSend(src, resync, false);
            }
        } else if (m.getType() == UDPMessageType.SYNC) {
            System.out.println("SYNC");
            UDPMessage syncAck;
            if (_receiving_sequence_number_mapping.containsKey(src)) {
                syncAck = new UDPMessage(-1, _receiving_sequence_number_mapping.get(src), null, UDPMessageType.SYNC_ACK);
            } else {
                _receiving_sequence_number_mapping.put(src, -1);
                syncAck = new UDPMessage(-1, -1, null, UDPMessageType.SYNC_ACK);
            }
            RawSend(src, syncAck, false);
        } else if (m.getType() == UDPMessageType.SYNC_ACK) {
            System.out.println("SYNC_ACK");
            if (_timeout_handlers.containsKey(src)) {
                TimeoutHandler temp = _timeout_handlers.remove(src);
                temp.Stop();
                int newSeq = m.getAckNumber() + 1;
                _sending_sequence_number_mapping.put(src, newSeq);
                if (!_sending_queue.get(src).isEmpty()) {
                    QueuedSend qs = _sending_queue.get(src).remove();
                    UDPMessage mTemp = new UDPMessage(_sending_sequence_number_mapping.get(src), -1, qs.data, UDPMessageType.DATA, qs.associatedResult);
                    RawSend(src, mTemp, true);
                }
            }
        } else if (m.getType() == UDPMessageType.RESYNC_REQU) {
            System.out.println("RESYNC_REQU");
            if (_timeout_handlers.containsKey(src)) {
                TimeoutHandler temp = _timeout_handlers.remove(src);
                temp.Stop();
                addToSendingQueue(src, temp.getAssociatedMessage().getData(), temp.getAssociatedMessage().getAssociatedResult());
                UDPMessage sync = new UDPMessage(-1, -1, null, UDPMessageType.SYNC);
                RawSend(src, sync, true);
            }
        }
    }

}
