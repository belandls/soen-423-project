/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author Samuel
 */
public class UDPMessage implements Serializable {

    private int _sequence_number;
    private int _ack_number;
    private Serializable _data;
    private UDPMessageType _type;
    private transient UDPCommunicator.DeferredSendResult _associated_res;
    private transient int _resent_count;

    public UDPMessage() {
    }

    public UDPMessage(int sequenceNumber, int ackNumber, Serializable data, UDPMessageType type) {
        _sequence_number = sequenceNumber;
        _ack_number = ackNumber;
        _data = data;
        _type = type;
    }

    public UDPMessage(int sequenceNumber, int ackNumber, Serializable data, UDPMessageType type, UDPCommunicator.DeferredSendResult res) {
        this(sequenceNumber, ackNumber, data, type);
        _associated_res = res;
    }

    public int getSequenceNumber() {
        return _sequence_number;
    }

    public int getAckNumber() {
        return _ack_number;
    }

    public Serializable getData() {
        return _data;
    }

    public UDPMessageType getType() {
        return _type;
    }
    
    public UDPCommunicator.DeferredSendResult getAssociatedResult(){
        return _associated_res;
    }
    
    public int getResentCount(){
        return _resent_count;
    }

    public void incrementResent(){
        ++_resent_count;
    }
    
    public byte[] ToBytes() {
        try {
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            ObjectOutputStream o = new ObjectOutputStream(b);
            o.writeObject(this);
            byte[] res = b.toByteArray();
            o.close();
            return res;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public static UDPMessage FromBytes(byte[] data) {
        try {
            ByteArrayInputStream b = new ByteArrayInputStream(data);
            ObjectInputStream o = new ObjectInputStream(b);
            UDPMessage res = (UDPMessage) o.readObject();
            o.close();
            return res;
        } catch (Exception ex) {

        }
        return null;
    }
}
