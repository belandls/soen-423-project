/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

import java.util.Date;



/**
 *
 * @author Samuel
 */
public class TimeoutHandler implements Runnable{
    
    public static int TIMEOUT_TIME = 500;
    
    private UDPMessage _buffered_message;
    private Host _associated_host;
    private ITimeoutHandler _timeout_handler;
    private boolean _should_check;
    
    public TimeoutHandler(Host h, UDPMessage m, ITimeoutHandler toh){
        _associated_host = h;
        _buffered_message = m;
        _timeout_handler = toh;
        _should_check = true;
    }

    public void Stop(){
        _should_check = false;
    }
    
    public UDPMessage getAssociatedMessage(){
        return _buffered_message;
    }
    
    @Override
    public void run() {
        long initialTime = System.currentTimeMillis();
        while(_should_check){
            try{
                Thread.sleep(50);
            }catch(Exception ex){
                
            }
            long diff = System.currentTimeMillis() - initialTime;
            if(diff > TIMEOUT_TIME){
                _should_check = false;
                _timeout_handler.TimedOut(_associated_host, _buffered_message);
            }
        }
    }
    
    
}
