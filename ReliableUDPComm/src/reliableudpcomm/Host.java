/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

import java.net.InetAddress;
import java.util.Objects;

/**
 *
 * @author Samuel
 */
public class Host {
    private String _hostname;
    private int _port;
    
    public Host(InetAddress hn, int port){
        _hostname = hn.getHostAddress();
        _port = port;
    }
    
    public String getHostname(){
        return _hostname;
    }
    
    public int getPort(){
        return _port;
    }
    
    @Override
    public int hashCode(){
        return _hostname.hashCode() + _port;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Host other = (Host) obj;
        if (this._port != other._port) {
            return false;
        }
        if (!Objects.equals(this._hostname, other._hostname)) {
            return false;
        }
        return true;
    }
    
}
