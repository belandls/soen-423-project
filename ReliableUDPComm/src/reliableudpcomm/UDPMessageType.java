/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

/**
 *
 * @author Samuel
 */
public enum UDPMessageType {
    DATA,
    SYNC,
    SYNC_ACK,
    RESYNC_REQU,
    ACK
}
