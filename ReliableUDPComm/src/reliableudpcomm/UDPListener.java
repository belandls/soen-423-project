/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reliableudpcomm;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;

/**
 *
 * @author Samuel
 */
public class UDPListener implements Runnable {

    private static final int BUFFER_SIZE = 2048;

    private IReceivedHandler _recv_handler;
    private boolean _should_listen;
    private DatagramSocket _socket;

    public UDPListener(IReceivedHandler receiveHandler, DatagramSocket sock) {
        _recv_handler = receiveHandler;
        _should_listen = true;
        _socket = sock;
        try {
            _socket.setSoTimeout(1000);
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public void stopListening() {
        _should_listen = false;
    }

    @Override
    public void run() {

        while (_should_listen) {
            try {
                byte[] buffer = new byte[BUFFER_SIZE];
                DatagramPacket temp = new DatagramPacket(buffer, BUFFER_SIZE);
                _socket.receive(temp);
                UDPMessage recvMessage = UDPMessage.FromBytes(temp.getData());
                Host src = new Host(temp.getAddress(), temp.getPort());
                _recv_handler.UDPMessageReceived(src, recvMessage);

            }catch(SocketTimeoutException ex){
                int i = 0;
            } catch (Exception ex) {
                ex.printStackTrace();
                _should_listen = false;
            }
        }

    }

}
