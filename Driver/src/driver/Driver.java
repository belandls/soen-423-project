/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package driver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import reliableudpcomm.Host;
import reliableudpcomm.UDPCommunicator;
import reliableudpcomm.UDPSendResult;

/**
 *
 * @author Sam
 */
public class Driver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SocketException, UnknownHostException, IOException {
        UDPCommunicator comm = new UDPCommunicator(Integer.parseInt(args[1]));
        comm.startListening();
        if(args[0].equals("0")){
            String recvData = (String)comm.Receive();
            System.out.println(recvData);
            
            recvData = (String)comm.Receive();
            System.out.println(recvData);
        }else{
            UDPCommunicator.DeferredSendResult res = comm.Send(new Host(InetAddress.getByName("localhost"), 12345), "Allo");
            res.WaitForResult();
            System.in.read();
            res = comm.Send(new Host(InetAddress.getByName("localhost"), 12345), "benanas");
            res.WaitForResult();
            if(res.getResult() == UDPSendResult.OK){
                System.out.println("Sent OK");
            }else{
                System.out.println("Timed Out");
            }
        }
        comm.shutdown();
        
    }
    
}
